import java.io.*;

class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
         System.out.print("Enter the value of rows: ");
	int rows=Integer.parseInt(br.readLine());
        char[][] pattern = new char[rows][rows];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                if (i % 2 == 0) {
                    pattern[i][j] = (char)('A' + j);
                } else {
                    pattern[i][j] = (char)('A' + rows - 1 - j);
                }
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < rows; j++) {
                System.out.print(pattern[i][j] + " ");
            }
            System.out.println();
        }
    }
}

