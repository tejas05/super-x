import java.io.*;

class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter the start number: ");
        String startStr = reader.readLine();
        int start = Integer.parseInt(startStr);

        System.out.print("Enter the end number: ");
        String endStr = reader.readLine();
        int end = Integer.parseInt(endStr);

        if (start <= end) {
            System.out.println("Reverse numbers in the range:");
            for (int i = start; i <= end; i++) {
                int rev = temp(i);
                System.out.println(rev);
            }
        } else {
            System.out.println("Invalid input: The start number should be less than or equal to the end number.");
        }
    }

    public static int temp(int number) {
        int temp = 0;

        while (number > 0) {
            int digit = number % 10;
            temp = temp * 10 + digit;
            number /= 10;
        }

        return temp;
    }
}

