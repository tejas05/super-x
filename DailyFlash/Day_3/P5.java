import java.io.*;

class Demo {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter a string: ");
        String input = br.readLine();

        if (containsNonLetters(input)) {
            System.out.println("The string contains characters other than letters.");
        } else {
            System.out.println("The string contains only letters.");
        }
    }

    public static boolean containsNonLetters(String input) {
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (!Character.isLetter(c)) {
                return true; 
            }
        }
        return false; 
    }
}

