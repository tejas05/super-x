import java.io.*;

class Demo {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the Rows:");
        int row = Integer.parseInt(br.readLine());

        Pattern(row);
    }

    public static void Pattern(int numRows) {
        for (int i = 1; i <= numRows; i++) {
            int x = i;
            for (int j = 1; j <= numRows; j++) {
                System.out.print(x + " ");
                x++;
            }
            System.out.println();
        }
    }
}


