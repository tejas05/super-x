import java.io.*;

class Demo {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the number of rows: ");
        int rows = Integer.parseInt(br.readLine());

        Pattern(rows);
    }

    public static void Pattern(int numRows) {
        int num = 1;

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print((num - i) + " ");
                num++;
            }
            System.out.println();
        }
    }
}

