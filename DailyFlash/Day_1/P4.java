import java.util.Scanner;
class Odd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the start: ");
        int start = scanner.nextInt();
        System.out.print("Enter the end: ");
        int end = scanner.nextInt();

        printOddNumbersInRange(start, end);
    }

    public static void printOddNumbersInRange(int start, int end) {
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}


