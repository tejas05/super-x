import java.util.Scanner;

class Count {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String inputString = scanner.nextLine();

        int length = 0;

        for (int i = 0; i < inputString.length(); i++) {
            length++;
        }

        System.out.println("The size of the string is: " + length);
    }
}

