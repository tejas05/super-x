import java.io.*;

class Demo {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the Rows:");
        int row = Integer.parseInt(br.readLine());

        Pattern(row);
    }

    public static void Pattern(int numRows) {
	    char ch='A';
        for (int i = 1; i <= numRows; i++) {
            char temp=ch;
            for (int j = 1; j <= numRows; j++) {
                System.out.print(temp + " ");
                temp++;
            }
	    ch++;
            System.out.println();
        }
    }
}

