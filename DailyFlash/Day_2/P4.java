import java.util.Scanner;
class CompositeN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the start value: ");
        int start = scanner.nextInt();

        System.out.print("Enter the end value: ");
        int end = scanner.nextInt();

        CompositeNumbers(start, end);
    }

    public static void CompositeNumbers(int start, int end) {
        for (int num = start; num <= end; num++) {
            if (Composite(num)) {
                System.out.print(num + " ");
            }
        }
    }

    public static boolean Composite(int n) {
        if (n <= 1) {
            return false;
        }
        if (n == 2 || n == 3) {
            return false;
        }
        if (n % 2 == 0 || n % 3 == 0) {
            return true;
        }

        for (int i = 5; i * i <= n; i += 6) {
            if (n % i == 0 || n % (i + 2) == 0) {
                return true;
            }
        }

        return false;
    }
}

